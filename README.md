# Revolut Interview App

*Android single activity app for real-time rate changes.*

###### Note for interviewer:
* Rate icons and names were not provided by API, thus are being hardcoded
in resources.

* Due to lack of specification regarding error handling, it's implemented
in a straightforward manner showing a dedicated screen on any network error
which resolves itself automatically.
(IMHO showing any cached content during a network failure might be misleading.)

* Due to lack of concrete specification regarding test coverage, the codebase is
covered with unit tests for non-UI functionality, and automated tests for UI.

* Project structure is prepared for further development as integration with CI
(regression suit).

* Screenshots from the app


![App screen](screens/app.jpg)
![App screen](screens/error.jpg)
