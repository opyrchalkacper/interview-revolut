package com.opyrchalkacper.interview.revolut.repository

import com.opyrchalkacper.interview.revolut.model.Currency
import com.opyrchalkacper.interview.revolut.remote.ApiService
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.rxkotlin.Observables
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class RatesStreamer @Inject constructor(
    private val apiService: ApiService
) {

    fun latestRates(
        baseUpdates: Observable<Currency>
    ): Observable<Rates> =
        Observables.combineLatest(
            baseUpdates,
            Observable.interval(0, 1, TimeUnit.SECONDS)
        ) { base, _ -> base }
            .switchMap { base ->
                fetchCurrencyTable(base).map { currencyList ->
                    Rates(
                        base = base,
                        currencies = currencyList
                    )
                }.toObservable()
            }

    private fun fetchCurrencyTable(base: Currency): Single<List<Currency>> =
        apiService.latestCurrencies(base.symbol.name)
            .subscribeOn(Schedulers.io())
            .map { latestCurrencies ->
                latestCurrencies.rates.map {
                    Currency(
                        symbol = it.key,
                        value = it.value.toBigDecimal()
                    )
                }
            }
            .onErrorResumeNext { Single.just(emptyList()) }
}