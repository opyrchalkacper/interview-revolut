package com.opyrchalkacper.interview.revolut.ui

import android.content.Context
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.recyclerview.widget.RecyclerView
import com.opyrchalkacper.interview.revolut.R
import com.opyrchalkacper.interview.revolut.model.Currency
import de.hdodenhof.circleimageview.CircleImageView
import io.reactivex.disposables.Disposables
import kotlinx.android.synthetic.main.currency_list_item.view.*


class CurrenciesListViewAdapter(
    private var currencies: MutableList<Currency>,
    private val presenter: CurrencyItemsPresenter,
    private val scroll: () -> Unit
) : RecyclerView.Adapter<CurrencyViewHolder>(), CurrenciesAdapter {

    fun updateDataSet(latestCurrencies: MutableList<Currency>) {
        latestCurrencies.forEach { latestCurrency ->
            currencies.find { latestCurrency.symbol == it.symbol }!!.value = latestCurrency.value
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.currency_list_item, parent, false)
        return CurrencyViewHolder(
            view = view,
            presenter = presenter
        )
    }

    override fun getItemCount(): Int = currencies.size

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        val item = currencies[position]
        holder.setCurrency(item)

        with(holder.view) {
            tag = item
            val onItemClick = {
                presenter.onItemClicked(
                    adapter = this@CurrenciesListViewAdapter,
                    view = holder,
                    currencies = currencies,
                    symbolName = item.symbol.symbolName
                )
            }
            currencyInput.setOnTouchListener { _, event ->
                if (MotionEvent.ACTION_UP == event.action) {
                    onItemClick()
                }
                false
            }
            setOnClickListener { onItemClick() }
        }
    }

    override fun scrollToTop() {
        scroll()
    }

    override fun onViewAttachedToWindow(holder: CurrencyViewHolder) {
        super.onViewAttachedToWindow(holder)
        holder.onAttach()
    }

    override fun onViewDetachedFromWindow(holder: CurrencyViewHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.onDetach()
    }
}

data class CurrencyViewHolder(
    val view: View,
    val currencySymbol: TextView = view.currencySymbol,
    val currencyInput: EditText = view.currencyInput,
    val currencyName: TextView = view.currencyName,
    val currencyFlagImage: CircleImageView = view.currencyFlagImage,
    val presenter: CurrencyItemsPresenter
) : RecyclerView.ViewHolder(view), CurrencyItemView {

    private var disposable = Disposables.disposed()
    private lateinit var currency: Currency

    fun onAttach() {
        disposable = presenter.onAttach(this, currency)

        currencyInput.apply {
            filters = arrayOf(CurrenciesDecimalPlacesInputFilter)
            setOnFocusChangeListener { _, focus ->
                presenter.onAmountFieldFocusChanged(focus, this@CurrencyViewHolder)
            }
        }
    }

    fun onDetach() {
        presenter.onDetach(disposable)
    }

    fun setCurrency(currency: Currency) {
        this.currency = currency
    }

    override fun addAmountChangeListener() {
        currencyInput.addTextChangedListener(amountChangedListener)
    }

    override fun removeAmountChangeListener() {
        currencyInput.removeTextChangedListener(amountChangedListener)
    }

    override fun requestFocus() {
        currencyInput.requestFocus()
    }

    override fun showKeyboard() {
        (view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).run {
            showSoftInput(currencyInput, InputMethodManager.RESULT_SHOWN)
        }
    }

    override fun updateValue(text: String) {
        currency = currency.copy(value = text.toBigDecimal())
        currencyInput.text = SpannableStringBuilder(text)
    }

    override fun setSymbol(text: String) {
        currencySymbol.text = text
    }

    override fun setFlagImage(@DrawableRes flag: Int) {
        currencyFlagImage.setImageResource(flag)
    }

    override fun setCurrencyName(@StringRes name: Int) {
        currencyName.setText(name)
    }

    private val amountChangedListener = object : TextWatcher {

        override fun afterTextChanged(s: Editable?) {}

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            presenter.onAmountTextChanged(s.toString(), currency.symbol)
        }
    }
}