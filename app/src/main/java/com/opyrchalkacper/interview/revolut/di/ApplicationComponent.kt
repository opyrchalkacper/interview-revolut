package com.opyrchalkacper.interview.revolut.di

import com.opyrchalkacper.interview.revolut.MainActivity
import com.opyrchalkacper.interview.revolut.ui.CurrenciesFragment
import dagger.Component
import okhttp3.Dispatcher
import javax.inject.Scope

@ApplicationScope
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    fun inject(activity: MainActivity)
    fun inject(fragment: CurrenciesFragment)

    fun okHttpDispatcher(): Dispatcher
}

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ApplicationScope