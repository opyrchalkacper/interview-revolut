package com.opyrchalkacper.interview.revolut.model

import java.math.BigDecimal

data class Currency(
    val symbol: CurrencySymbol,
    var value: BigDecimal
)