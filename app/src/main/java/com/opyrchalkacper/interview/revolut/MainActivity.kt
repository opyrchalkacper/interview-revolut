package com.opyrchalkacper.interview.revolut

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.opyrchalkacper.interview.revolut.ui.CurrenciesFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.elevation = 0f

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.viewHolder, CurrenciesFragment(), "currencies_fragment")
                .commitAllowingStateLoss()
        }
    }
}
