package com.opyrchalkacper.interview.revolut.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.opyrchalkacper.interview.revolut.R
import com.opyrchalkacper.interview.revolut.RevolutApplication
import com.opyrchalkacper.interview.revolut.model.Currency
import kotlinx.android.synthetic.main.fragment_currencies.*
import javax.inject.Inject

class CurrenciesFragment : androidx.fragment.app.Fragment(), CurrenciesView {

    @Inject
    lateinit var currenciesPresenter: CurrenciesPresenter

    @Inject
    lateinit var currencyItemsPresenter: CurrencyItemsPresenter

    private lateinit var linearLayoutManager: LinearLayoutManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_currencies, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        RevolutApplication.applicationComponent.inject(this)

        linearLayoutManager = LinearLayoutManager(context)
        currenciesList.layoutManager = linearLayoutManager

        currenciesPresenter.attachView(this)
    }

    override fun onDestroyView() {
        currenciesPresenter.detachView()
        super.onDestroyView()
    }

    override fun hideKeyboard() {
        (activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).run {
            hideSoftInputFromWindow(view?.windowToken, 0)
        }
    }

    override fun setItems(items: MutableList<Currency>) {
        with(currenciesList) {
            if (adapter == null) {
                adapter = CurrenciesListViewAdapter(items, currencyItemsPresenter) {
                    linearLayoutManager.scrollToPositionWithOffset(0, 0)
                }
                setOnScrollChangeListener { _, _, _, _, _ ->
                    currenciesPresenter.onCurrenciesScrolled(
                        linearLayoutManager.findFirstVisibleItemPosition(),
                        this@CurrenciesFragment
                    )
                }
            } else {
                (adapter as CurrenciesListViewAdapter).updateDataSet(items)
            }
        }
    }

    override fun showError() {
        errorView.visibility = VISIBLE
        currenciesList.visibility = GONE
    }

    override fun hideError() {
        errorView.visibility = GONE
    }

    override fun setProgress(show: Boolean) {
        progress.visibility = if (show) VISIBLE else GONE
        currenciesList.visibility = if (show) GONE else VISIBLE
    }
}