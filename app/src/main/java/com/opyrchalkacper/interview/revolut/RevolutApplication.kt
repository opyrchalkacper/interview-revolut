package com.opyrchalkacper.interview.revolut

import android.app.Application
import com.facebook.stetho.Stetho
import com.opyrchalkacper.interview.revolut.di.ApplicationComponent
import com.opyrchalkacper.interview.revolut.di.ApplicationModule
import com.opyrchalkacper.interview.revolut.di.DaggerApplicationComponent

open class RevolutApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)

        }
        applicationComponent = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule())
            .build()
    }

    companion object {
        lateinit var applicationComponent: ApplicationComponent
    }
}