package com.opyrchalkacper.interview.revolut.repository

import com.opyrchalkacper.interview.revolut.di.ApplicationScope
import com.opyrchalkacper.interview.revolut.model.Currency
import com.opyrchalkacper.interview.revolut.model.CurrencySymbol.EUR
import com.opyrchalkacper.interview.revolut.ui.DECIMAL_PLACES
import io.reactivex.Observable
import io.reactivex.rxkotlin.Observables
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.math.BigDecimal
import java.math.RoundingMode
import javax.inject.Inject

data class Rates(
    val base: Currency,
    val currencies: List<Currency>
)

sealed class RatesResult {
    class Ok(val rates: Rates) : RatesResult()

    object Error : RatesResult()
}

private val INIT_CURRENCY_AMOUNT = BigDecimal.ONE

@ApplicationScope
class RatesProvider @Inject constructor(ratesStreamer: RatesStreamer) {

    private val updates = BehaviorSubject.createDefault(Currency(EUR, INIT_CURRENCY_AMOUNT))

    fun update(currency: Currency) {
        updates.onNext(currency)
    }

    val latestRates: Observable<RatesResult> =
        Observables.combineLatest(
            ratesStreamer.latestRates(baseCurrencyChange()),
            updates
        ) { rates, base -> rates to base }
            .subscribeOn(Schedulers.io())
            .filter(filterInvalidUpdates())
            .map { (rates, base) ->
                if (rates.currencies.isEmpty()) {
                    RatesResult.Error
                } else {
                    RatesResult.Ok(
                        rates = Rates(
                            base = base,
                            currencies = calculateRatesForBase(base, rates.currencies)
                        )
                    )
                }
            }
            .replay(1)
            .refCount()

    private fun filterInvalidUpdates(): (Pair<Rates, Currency>) -> Boolean {
        return { (rates, base) ->
            rates.base.symbol == base.symbol
        }
    }

    private fun baseCurrencyChange() =
        updates.distinctUntilChanged { base: Currency -> base.symbol }

    private fun calculateRatesForBase(
        base: Currency,
        currencies: List<Currency>
    ): List<Currency> {
        return currencies.map { currency ->
            val value =
                (currency.value * base.value).setScale(DECIMAL_PLACES, RoundingMode.HALF_EVEN)
                    .stripTrailingZeros()
            Currency(
                symbol = currency.symbol,
                value = value
            )
        }
    }
}