package com.opyrchalkacper.interview.revolut.ui

import com.opyrchalkacper.interview.revolut.model.Currency
import com.opyrchalkacper.interview.revolut.repository.RatesProvider
import com.opyrchalkacper.interview.revolut.repository.RatesResult
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposables
import javax.inject.Inject

class CurrenciesPresenter @Inject constructor(
    private val ratesProvider: RatesProvider
) {

    private var disposable = Disposables.disposed()

    fun attachView(view: CurrenciesView) {
        disposable = with(view) {
            setProgress(true)
            ratesProvider.latestRates
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { result -> onNextResult(result) }
        }
    }

    fun onCurrenciesScrolled(firstVisibleItemPosition: Int, view: CurrenciesView) {
        if (firstVisibleItemPosition > 0) {
            view.hideKeyboard()
        }
    }

    private fun CurrenciesView.onNextResult(result: RatesResult?) {
        setProgress(false)
        when (result) {
            is RatesResult.Ok -> {
                hideError()
                val items = mutableListOf(result.rates.base)
                items.addAll(result.rates.currencies)
                setItems(items)
            }
            is RatesResult.Error -> {
                hideKeyboard()
                showError()
            }
        }
    }

    fun detachView() {
        disposable.dispose()
    }
}

interface CurrenciesView {
    fun setItems(items: MutableList<Currency>)
    fun showError()
    fun hideError()
    fun setProgress(show: Boolean)
    fun hideKeyboard()
}