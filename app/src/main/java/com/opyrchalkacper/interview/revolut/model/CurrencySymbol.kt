package com.opyrchalkacper.interview.revolut.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.opyrchalkacper.interview.revolut.R

enum class CurrencySymbol(
    val symbolName: String,
    @DrawableRes val flagIcon: Int,
    @StringRes val fullName: Int
) {
    EUR(symbolName = "EUR", flagIcon = R.drawable.eur, fullName = R.string.eur_name),
    PLN(symbolName = "PLN", flagIcon = R.drawable.pln, fullName = R.string.pln_name),
    USD(symbolName = "USD", flagIcon = R.drawable.usd, fullName = R.string.usd_name),
    AUD(symbolName = "AUD", flagIcon = R.drawable.aud, fullName = R.string.aud_name),
    BGN(symbolName = "BGN", flagIcon = R.drawable.bgn, fullName = R.string.bgn_name),
    CHF(symbolName = "CHF", flagIcon = R.drawable.chf, fullName = R.string.chf_name),
    BRL(symbolName = "BRL", flagIcon = R.drawable.brl, fullName = R.string.brl_name),
    CAD(symbolName = "CAD", flagIcon = R.drawable.cad, fullName = R.string.cad_name),
    CZK(symbolName = "CZK", flagIcon = R.drawable.czk, fullName = R.string.czk_name),
    HKD(symbolName = "HKD", flagIcon = R.drawable.hkd, fullName = R.string.hkd_name),
    CNY(symbolName = "CNY", flagIcon = R.drawable.cny, fullName = R.string.cny_name),
    DKK(symbolName = "DKK", flagIcon = R.drawable.dkk, fullName = R.string.dkk_name),
    SGD(symbolName = "SGD", flagIcon = R.drawable.sgd, fullName = R.string.sgd_name),
    GBP(symbolName = "GBP", flagIcon = R.drawable.gbp, fullName = R.string.gbp_name),
    ILS(symbolName = "ILS", flagIcon = R.drawable.ils, fullName = R.string.ils_name),
    HRK(symbolName = "HRK", flagIcon = R.drawable.hrk, fullName = R.string.hrk_name),
    INR(symbolName = "INR", flagIcon = R.drawable.inr, fullName = R.string.inr_name),
    HUF(symbolName = "HUF", flagIcon = R.drawable.huf, fullName = R.string.huf_name),
    IDR(symbolName = "IDR", flagIcon = R.drawable.idr, fullName = R.string.idr_name),
    ISK(symbolName = "ISK", flagIcon = R.drawable.isk, fullName = R.string.isk_name),
    TRY(symbolName = "TRY", flagIcon = R.drawable.try_c, fullName = R.string.try_name),
    JPY(symbolName = "JPY", flagIcon = R.drawable.jpy, fullName = R.string.jpy_name),
    NZD(symbolName = "NZD", flagIcon = R.drawable.nzd, fullName = R.string.nzd_name),
    KRW(symbolName = "KRW", flagIcon = R.drawable.krw, fullName = R.string.krw_name),
    MXN(symbolName = "MXN", flagIcon = R.drawable.mxn, fullName = R.string.mxn_name),
    MYR(symbolName = "MYR", flagIcon = R.drawable.myr, fullName = R.string.myr_name),
    NOK(symbolName = "NOK", flagIcon = R.drawable.nok, fullName = R.string.nok_name),
    PHP(symbolName = "PHP", flagIcon = R.drawable.php, fullName = R.string.php_name),
    RON(symbolName = "RON", flagIcon = R.drawable.ron, fullName = R.string.ron_name),
    ZAR(symbolName = "ZAR", flagIcon = R.drawable.zar, fullName = R.string.zar_name),
    THB(symbolName = "THB", flagIcon = R.drawable.thb, fullName = R.string.thb_name),
    RUB(symbolName = "RUB", flagIcon = R.drawable.rub, fullName = R.string.rub_name),
    SEK(symbolName = "SEK", flagIcon = R.drawable.sek, fullName = R.string.sek_name)
}