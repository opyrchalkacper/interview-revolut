package com.opyrchalkacper.interview.revolut.ui

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.opyrchalkacper.interview.revolut.di.ApplicationScope
import com.opyrchalkacper.interview.revolut.model.Currency
import com.opyrchalkacper.interview.revolut.model.CurrencySymbol
import com.opyrchalkacper.interview.revolut.repository.Rates
import com.opyrchalkacper.interview.revolut.repository.RatesProvider
import com.opyrchalkacper.interview.revolut.repository.RatesResult
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import java.math.BigDecimal
import javax.inject.Inject

@ApplicationScope
class CurrencyItemsPresenter @Inject constructor(
    private val ratesProvider: RatesProvider
) {

    fun onAttach(view: CurrencyItemView, currency: Currency): Disposable =
        with(view) {
            setSymbol(currency.symbol.symbolName)
            setFlagImage(currency.symbol.flagIcon)
            setCurrencyName(currency.symbol.fullName)
            updateValue(currency.value.toPlainString())

            ratesProvider.latestRates
                .filter { it is RatesResult.Ok && currency.symbol != it.rates.base.symbol }
                .map { result ->
                    (result as RatesResult.Ok).rates.findCurrency(currency.symbol.symbolName)!!
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    updateValue(it.value.toPlainString())
                }
        }

    fun onDetach(disposable: Disposable) {
        disposable.dispose()
    }

    fun onAmountTextChanged(text: String, symbol: CurrencySymbol) {
        val amount = text.toBigDecimalOrNull() ?: BigDecimal.ZERO
        ratesProvider.update(Currency(symbol = symbol, value = amount))
    }

    fun onAmountFieldFocusChanged(hasFocus: Boolean, view: CurrencyItemView) {
        if (hasFocus) {
            view.addAmountChangeListener()
        } else {
            view.removeAmountChangeListener()
        }
    }

    fun onItemClicked(
        adapter: CurrenciesAdapter,
        view: CurrencyItemView,
        currencies: MutableList<Currency>,
        symbolName: String
    ) {
        val currency = currencies.find { it.symbol.symbolName == symbolName }!!
        val swapPosition = currencies.indexOf(currency)

        currencies.remove(currency)
        currencies.add(0, currency)

        adapter.notifyItemMoved(swapPosition, 0)
        adapter.scrollToTop()

        view.requestFocus()
        view.showKeyboard()

        ratesProvider.update(currency)
    }


    private fun Rates.findCurrency(symbolName: String) =
        currencies.find { it.symbol.symbolName == symbolName }
}

interface CurrencyItemView {
    fun updateValue(text: String)
    fun setSymbol(text: String)
    fun setFlagImage(@DrawableRes flag: Int)
    fun setCurrencyName(@StringRes name: Int)
    fun addAmountChangeListener()
    fun removeAmountChangeListener()
    fun requestFocus()
    fun showKeyboard()
}

interface CurrenciesAdapter {
    fun notifyItemMoved(from: Int, to: Int)
    fun scrollToTop()
}