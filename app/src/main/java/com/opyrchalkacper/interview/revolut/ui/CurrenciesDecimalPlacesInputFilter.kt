package com.opyrchalkacper.interview.revolut.ui


import android.text.InputFilter
import android.text.Spanned

const val DECIMAL_PLACES = 2

object CurrenciesDecimalPlacesInputFilter : InputFilter {

    override fun filter(
        source: CharSequence,
        start: Int,
        end: Int,
        dest: Spanned,
        dstart: Int,
        dend: Int
    ): CharSequence? {

        val sourceValue = source.toString()
        val destValue = dest.toString()
        val result = destValue.substring(0, dstart) +
                sourceValue.substring(start, end) +
                destValue.substring(dend)

        if (result.isEmpty()
            || sourceValue.substring(start, end).isEmpty()
            || checkDecimalPlaces(result)
        ) return null

        return when {
            destValue.isNotEmpty() -> destValue.substring(dstart, dend)
            sourceValue.isNotEmpty() -> maxDecimalPlacesValue(sourceValue, start)
            else -> null
        }
    }

    private fun checkDecimalPlaces(value: String): Boolean {
        val i = value.indexOf('.')
        return if (i < 0) true
        else value.length - 1 - i <= DECIMAL_PLACES
    }

    private fun maxDecimalPlacesValue(value: String, start: Int): String? {
        val i = value.indexOf('.')
        return if (i > 0) {
            val end = i + DECIMAL_PLACES + 1
            value.substring(start, end)
        } else null
    }
}