package com.opyrchalkacper.interview.revolut.remote

import com.opyrchalkacper.interview.revolut.model.LatestCurrencies
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("latest")
    fun latestCurrencies(@Query("base") base: String): Single<LatestCurrencies>

}