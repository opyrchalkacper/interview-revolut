package com.opyrchalkacper.interview.revolut.model

data class LatestCurrencies(
    val base: CurrencySymbol,
    val rates: LinkedHashMap<CurrencySymbol, String>
)