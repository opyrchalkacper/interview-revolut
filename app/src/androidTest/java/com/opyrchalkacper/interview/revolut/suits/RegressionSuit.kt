package com.opyrchalkacper.interview.revolut.suits

import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(
    CurrenciesListSuit::class
)
class RegressionSuit
