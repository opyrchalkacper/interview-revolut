package com.opyrchalkacper.interview.revolut.idlingresources

import androidx.test.espresso.IdlingResource
import okhttp3.Dispatcher

class OkHttpIdlingResource constructor(
    private val name: String,
    private val dispatcher: Dispatcher
) : IdlingResource {

    @Volatile
    private var callback: IdlingResource.ResourceCallback? = null

    init {
        dispatcher.setIdleCallback {
            callback?.onTransitionToIdle()
        }
    }

    override fun getName(): String = name

    override fun isIdleNow(): Boolean {
        val idle = dispatcher.runningCallsCount() == 0
        callback?.let {
            if (idle) it.onTransitionToIdle()
        }
        return idle
    }

    override fun registerIdleTransitionCallback(callback: IdlingResource.ResourceCallback?) {
        this.callback = callback
    }
}