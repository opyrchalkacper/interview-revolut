package com.opyrchalkacper.interview.revolut

import androidx.test.espresso.IdlingRegistry
import com.opyrchalkacper.interview.revolut.idlingresources.OkHttpIdlingResource

class InstrumentationRevolutApplication : RevolutApplication() {

    override fun onCreate() {
        super.onCreate()

        instance = this

        registerIdleResources()
    }

    private fun registerIdleResources() {
        val idlingRegistry = IdlingRegistry.getInstance()
        idlingRegistry.register(
            OkHttpIdlingResource("OkHttp", applicationComponent.okHttpDispatcher())
        )
    }

    companion object {

        lateinit var instance: InstrumentationRevolutApplication
    }
}