package com.opyrchalkacper.interview.revolut.suits

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.opyrchalkacper.interview.revolut.MainActivity
import com.opyrchalkacper.interview.revolut.screens.CurrenciesListScreen
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class CurrenciesListSuit {

    @JvmField
    @Rule
    var activityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun moveItemToTheTopAfterCLick() {
        CurrenciesListScreen {
            checkItem(0, "EUR")
            clickOnItem("CNY")
            checkItem(0, "CNY")
            clickOnItem("AUD")
            checkItem(0, "AUD")
            clickOnItem("BGN")
            checkItem(0, "BGN")
            clickOnItem("EUR")
            checkItem(0, "EUR")
        }
    }
}
