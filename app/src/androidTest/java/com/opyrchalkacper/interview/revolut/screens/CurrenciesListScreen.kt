package com.opyrchalkacper.interview.revolut.screens

import android.view.View
import com.agoda.kakao.common.views.KView
import com.agoda.kakao.recycler.KRecyclerItem
import com.agoda.kakao.recycler.KRecyclerView
import com.agoda.kakao.screen.Screen
import com.opyrchalkacper.interview.revolut.R
import org.hamcrest.Matcher

object CurrenciesListScreen : Screen<CurrenciesListScreen>() {

    private class CurrencyListItem(parent: Matcher<View>) : KRecyclerItem<CurrencyListItem>(parent)

    private val currenciesListRecycleView = KRecyclerView(
        builder = { withId(R.id.currenciesList) },
        itemTypeBuilder = { itemType(CurrenciesListScreen::CurrencyListItem) }
    )

    fun checkItem(itemIndex: Int, currencySymbol: String) {
        currenciesListRecycleView.childAt<KRecyclerItem<KView>>(itemIndex) {
            hasDescendant {
                withId(R.id.currencySymbol)
                containsText(currencySymbol)
            }
        }
    }

    fun clickOnItem(currencySymbol: String) {
        currenciesListRecycleView.childWith<CurrencyListItem> {
            withDescendant {
                withId(R.id.currencySymbol)
                containsText(currencySymbol)
            }
        }.perform {
            click()
        }
    }

}