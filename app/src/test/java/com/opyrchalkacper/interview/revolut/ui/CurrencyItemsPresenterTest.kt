package com.opyrchalkacper.interview.revolut.ui

import com.google.common.truth.Truth.assertThat
import com.nhaarman.mockitokotlin2.*
import com.opyrchalkacper.interview.revolut.RxRule
import com.opyrchalkacper.interview.revolut.model.Currency
import com.opyrchalkacper.interview.revolut.model.CurrencySymbol
import com.opyrchalkacper.interview.revolut.model.CurrencySymbol.*
import com.opyrchalkacper.interview.revolut.repository.Rates
import com.opyrchalkacper.interview.revolut.repository.RatesProvider
import com.opyrchalkacper.interview.revolut.repository.RatesResult
import io.reactivex.subjects.BehaviorSubject
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.math.BigDecimal

@ExtendWith(RxRule::class)
internal class CurrencyItemsPresenterTest {

    private val rates = BehaviorSubject.create<RatesResult>()

    private val ratesProvider: RatesProvider = mock {
        on { latestRates } doReturn (rates)
    }

    private val view: CurrencyItemView = mock()
    private val adapter: CurrenciesAdapter = mock()

    private val presenter = CurrencyItemsPresenter(ratesProvider)

    @Test
    fun `update value with received data when onAttach`() {
        val plnInitValue = plnCurrencyUpdate.copy(value = 3.14.toBigDecimal())
        presenter.onAttach(view, plnInitValue)

        rates.onNext(ratesResult)

        with(inOrder(view)) {
            verify(view).setSymbol("PLN")
            verify(view).updateValue("3.14")
            verify(view).updateValue("2.99")
        }
    }

    @Test
    fun `do nothing when update do not contain expected currency`() {
        val plnInitValue = plnCurrencyUpdate.copy(symbol = BRL)
        presenter.onAttach(view, plnInitValue)
        reset(view)

        rates.onNext(ratesResult)

        verifyZeroInteractions(view)
    }

    @Test
    fun `do nothing on update error`() {
        val plnInitValue = plnCurrencyUpdate.copy(symbol = BRL)
        presenter.onAttach(view, plnInitValue)
        reset(view)

        rates.onNext(RatesResult.Error)

        verifyZeroInteractions(view)
    }

    @Test
    fun `update value with received data after each tick`() {
        val plnInitValue = plnCurrencyUpdate.copy(value = 3.14.toBigDecimal())
        presenter.onAttach(view, plnInitValue)
        reset(view)

        rates.onNext(ratesResult(3.45.toBigDecimal()))
        rates.onNext(ratesResult(4.12.toBigDecimal()))
        rates.onNext(ratesResult(2.89.toBigDecimal()))

        with(inOrder(view)) {
            verify(view).updateValue("3.45")
            verify(view).updateValue("4.12")
            verify(view).updateValue("2.89")
        }
    }

    @ParameterizedTest
    @MethodSource("valueUpdates")
    fun `update rates interactor with proper values`(
        text: String,
        symbol: CurrencySymbol,
        expectedResult: BigDecimal
    ) {
        presenter.onAmountTextChanged(text, symbol)

        verify(ratesProvider).update(
            Currency(
                symbol = symbol,
                value = expectedResult
            )
        )
        verifyNoMoreInteractions(ratesProvider)
        verifyZeroInteractions(view)
    }

    @Test
    fun `add amount change listener when view has focus`() {
        presenter.onAmountFieldFocusChanged(true, view)

        verify(view).addAmountChangeListener()
        verifyNoMoreInteractions(view)
    }

    @Test
    fun `remove amount change listener when view has no focus`() {
        presenter.onAmountFieldFocusChanged(false, view)

        verify(view).removeAmountChangeListener()
        verifyNoMoreInteractions(view)
    }

    @Test
    fun `do not update value if view detached`() {
        val plnInitValue = plnCurrencyUpdate.copy(value = 3.14.toBigDecimal())
        val disposable = presenter.onAttach(view, plnInitValue)
        reset(view)
        presenter.onDetach(disposable)

        rates.onNext(ratesResult(3.45.toBigDecimal()))
        rates.onNext(ratesResult(4.12.toBigDecimal()))

        verifyZeroInteractions(view)
    }

    @ParameterizedTest
    @MethodSource("currenciesInOrder")
    fun `move item with given symbol to top and notify view`(
        symbol: String,
        currency: Currency,
        swapPosition: Int,
        expectedOrder: MutableList<Currency>
    ) {
        val currencies = mutableListOf(
            plnCurrencyUpdate,
            usdCurrencyUpdate,
            cnyCurrencyUpdate
        )

        presenter.onItemClicked(adapter, view, currencies, symbol)

        assertThat(currencies).isEqualTo(expectedOrder)
        verify(adapter).notifyItemMoved(swapPosition, 0)
        verify(adapter).scrollToTop()
        verify(view).requestFocus()
        verify(view).showKeyboard()
    }

    @Test
    fun `fail fast when there is no item with given symbol`() {
        val currencies = mutableListOf(
            plnCurrencyUpdate,
            usdCurrencyUpdate,
            cnyCurrencyUpdate
        )

        assertThrows(NullPointerException::class.java) {
            presenter.onItemClicked(adapter, view, currencies, "no such symbol in the list :(")
        }
    }

    private companion object {

        @JvmStatic
        @Suppress("unused")
        fun valueUpdates() = arrayOf(
            arrayOf("1.23", USD, 1.23.toBigDecimal()),
            arrayOf("123456", PLN, 123456.toBigDecimal()),
            arrayOf(".9", EUR, 0.9.toBigDecimal()),
            arrayOf("0", GBP, 0.toBigDecimal()),
            arrayOf("", CNY, 0.toBigDecimal()),
            arrayOf("zero is the fallback value", BRL, 0.toBigDecimal())
        )

        @JvmStatic
        @Suppress("unused")
        fun currenciesInOrder() = arrayOf(
            arrayOf(
                "PLN",
                plnCurrencyUpdate,
                0,
                mutableListOf(plnCurrencyUpdate, usdCurrencyUpdate, cnyCurrencyUpdate)
            ),
            arrayOf(
                "USD",
                usdCurrencyUpdate,
                1,
                mutableListOf(usdCurrencyUpdate, plnCurrencyUpdate, cnyCurrencyUpdate)
            ),
            arrayOf(
                "CNY",
                cnyCurrencyUpdate,
                2,
                mutableListOf(cnyCurrencyUpdate, plnCurrencyUpdate, usdCurrencyUpdate)
            )
        )

        val baseCurrency = Currency(EUR, BigDecimal.ONE)
        val plnCurrencyUpdate = Currency(symbol = PLN, value = 2.99.toBigDecimal())
        val usdCurrencyUpdate = Currency(symbol = USD, value = 1.21.toBigDecimal())
        val cnyCurrencyUpdate = Currency(symbol = CNY, value = 2.74.toBigDecimal())


        val ratesResult = RatesResult.Ok(
            Rates(
                base = baseCurrency,
                currencies = listOf(
                    plnCurrencyUpdate,
                    usdCurrencyUpdate,
                    cnyCurrencyUpdate
                )
            )
        )

        fun ratesResult(plnValue: BigDecimal) = RatesResult.Ok(
            Rates(
                base = baseCurrency,
                currencies = listOf(
                    plnCurrencyUpdate.copy(value = plnValue),
                    usdCurrencyUpdate,
                    cnyCurrencyUpdate
                )
            )
        )
    }
}