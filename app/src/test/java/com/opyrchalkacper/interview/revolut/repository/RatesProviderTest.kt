package com.opyrchalkacper.interview.revolut.repository

import com.google.common.truth.Truth.assertThat
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.opyrchalkacper.interview.revolut.RxRule
import com.opyrchalkacper.interview.revolut.model.Currency
import com.opyrchalkacper.interview.revolut.model.CurrencySymbol.*
import com.opyrchalkacper.interview.revolut.model.LatestCurrencies
import com.opyrchalkacper.interview.revolut.remote.ApiService
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension
import java.math.BigDecimal
import java.util.concurrent.TimeUnit

internal class RatesProviderTest {

    private val testScheduler = TestScheduler()

    @RegisterExtension
    @JvmField
    @Suppress("unused")
    val rxRule = RxRule(testScheduler)

    private val api: ApiService = mock {
        on { latestCurrencies("EUR") } doReturn Single.just(latestCurrenciesEur)
        on { latestCurrencies("USD") } doReturn Single.just(latestCurrenciesUsd)
        on { latestCurrencies("AUD") } doReturn Single.error(Throwable())
    }

    private val ratesStreamer = RatesStreamer(api)
    private val ratesInteractor by lazy { RatesProvider(ratesStreamer) }

    @Test
    fun `should emit single rates list just after subscription`() {
        val test = ratesInteractor.latestRates.test()

        testScheduler.triggerActions()

        test.assertValue { (it as RatesResult.Ok).rates == expectedEurRates }
    }

    @Test
    fun `should emit second value after 1 second`() {
        val test = ratesInteractor.latestRates.test()

        testScheduler.advanceTimeBy(1, TimeUnit.SECONDS)
        testScheduler.triggerActions()

        val values = test.values()
        assertThat((values[0] as RatesResult.Ok).rates).isEqualTo(expectedEurRates)
        assertThat((values[1] as RatesResult.Ok).rates).isEqualTo(expectedEurRates)
        assertThat(values.size).isEqualTo(2)
    }

    @Test
    fun `should emit second value after update`() {
        val test = ratesInteractor.latestRates.test()

        testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)
        ratesInteractor.update(Currency(USD, BigDecimal.ONE))
        testScheduler.triggerActions()

        val values = test.values()
        assertThat((values[0] as RatesResult.Ok).rates).isEqualTo(expectedEurRates)
        assertThat((values[1] as RatesResult.Ok).rates).isEqualTo(expectedUsdRates)
        assertThat(values.size).isEqualTo(2)
    }

    @Test
    fun `should emit value with new base after update and continue emit new base with next ticks`() {
        val test = ratesInteractor.latestRates.test()

        testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)
        ratesInteractor.update(Currency(USD, BigDecimal.ONE))
        testScheduler.advanceTimeBy(1, TimeUnit.SECONDS)
        ratesInteractor.update(Currency(EUR, BigDecimal.ONE))
        testScheduler.advanceTimeBy(1, TimeUnit.SECONDS)
        testScheduler.triggerActions()

        val values = test.values()
        assertThat((values[0] as RatesResult.Ok).rates).isEqualTo(expectedEurRates)
        assertThat((values[1] as RatesResult.Ok).rates).isEqualTo(expectedUsdRates)
        assertThat((values[2] as RatesResult.Ok).rates).isEqualTo(expectedUsdRates)
        assertThat((values[3] as RatesResult.Ok).rates).isEqualTo(expectedEurRates)
        assertThat((values[4] as RatesResult.Ok).rates).isEqualTo(expectedEurRates)
        assertThat(values.size).isEqualTo(5)
    }


    @Test
    fun `should emit second value after update and calculate proper table`() {
        val expectedEurRatesAfterValueUpdate =
            Rates(
                base = Currency(EUR, 150.toBigDecimal()),
                currencies = listOf(
                    Currency(
                        symbol = PLN,
                        value = 36.toBigDecimal()
                    ),
                    Currency(
                        symbol = USD,
                        value = 144.toBigDecimal()
                    ),
                    Currency(
                        symbol = GBP,
                        value = 183.toBigDecimal()
                    )
                )
            )

        val test = ratesInteractor.latestRates.test()

        testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)
        ratesInteractor.update(Currency(EUR, 150.toBigDecimal()))
        testScheduler.triggerActions()

        val values = test.values()
        assertThat((values[0] as RatesResult.Ok).rates).isEqualTo(expectedEurRates)
        assertThat((values[1] as RatesResult.Ok).rates).isEqualTo(expectedEurRatesAfterValueUpdate)
        assertThat(values.size).isEqualTo(2)
    }

    @Test
    fun `should emit error value keep trying after each second`() {
        val test = ratesInteractor.latestRates.test()

        testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)
        ratesInteractor.update(Currency(AUD, BigDecimal.ONE))
        testScheduler.advanceTimeBy(2, TimeUnit.SECONDS)
        testScheduler.triggerActions()

        val values = test.values()
        assertThat((values[0] as RatesResult.Ok).rates).isEqualTo(expectedEurRates)
        assertThat(values[1]).isEqualTo(RatesResult.Error)
        assertThat(values[2]).isEqualTo(RatesResult.Error)
        assertThat(values[3]).isEqualTo(RatesResult.Error)
        assertThat(values.size).isEqualTo(4)
    }


    companion object {

        val expectedEurRates =
            Rates(
                base = Currency(EUR, BigDecimal.ONE),
                currencies = listOf(
                    Currency(
                        symbol = PLN,
                        value = 0.24.toBigDecimal()
                    ),
                    Currency(
                        symbol = USD,
                        value = 0.96.toBigDecimal()
                    ),
                    Currency(
                        symbol = GBP,
                        value = 1.22.toBigDecimal()
                    )
                )
            )

        val expectedUsdRates = Rates(
            base = Currency(USD, BigDecimal.ONE),
            currencies = listOf(
                Currency(
                    symbol = PLN,
                    value = 0.31.toBigDecimal()
                ),
                Currency(
                    symbol = EUR,
                    value = 1.06.toBigDecimal()
                ),
                Currency(
                    symbol = GBP,
                    value = 1.34.toBigDecimal()
                )
            )
        )

        val latestCurrenciesEur = LatestCurrencies(
            base = EUR,
            rates = linkedMapOf(
                PLN to "0.24",
                USD to "0.96",
                GBP to "1.22"
            )
        )

        val latestCurrenciesUsd = LatestCurrencies(
            base = USD,
            rates = linkedMapOf(
                PLN to "0.31",
                EUR to "1.06",
                GBP to "1.34"
            )
        )
    }
}