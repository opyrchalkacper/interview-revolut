package com.opyrchalkacper.interview.revolut.ui

import com.nhaarman.mockitokotlin2.*
import com.opyrchalkacper.interview.revolut.RxRule
import com.opyrchalkacper.interview.revolut.model.Currency
import com.opyrchalkacper.interview.revolut.model.CurrencySymbol
import com.opyrchalkacper.interview.revolut.repository.Rates
import com.opyrchalkacper.interview.revolut.repository.RatesProvider
import com.opyrchalkacper.interview.revolut.repository.RatesResult
import io.reactivex.subjects.BehaviorSubject
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import java.math.BigDecimal

@ExtendWith(RxRule::class)
internal class CurrenciesPresenterTest {

    private val rates = BehaviorSubject.create<RatesResult>()
    private val ratesProvider: RatesProvider = mock {
        on { latestRates } doReturn (rates)
    }
    private val view: CurrenciesView = mock()

    private val presenter = CurrenciesPresenter(ratesProvider)


    @Test
    fun `set items on view when rates tick and view attached`() {
        val expectedItems = mutableListOf(
            baseCurrency,
            plnCurrencyUpdate,
            usdCurrencyUpdate,
            cnyCurrencyUpdate
        )
        presenter.attachView(view)

        rates.onNext(ratesResult)

        with(inOrder(view)) {
            verify(view).setProgress(true)
            verify(view).setProgress(false)
            verify(view).hideError()
            verify(view).setItems(expectedItems)
        }
    }

    @Test
    fun `hide keyboard and show error on update error`() {
        presenter.attachView(view)

        rates.onNext(RatesResult.Error)

        with(inOrder(view)) {
            verify(view).setProgress(true)
            verify(view).setProgress(false)
            verify(view).hideKeyboard()
            verify(view).showError()
        }
    }

    @ParameterizedTest
    @CsvSource("1", "3", "45", "999")
    fun `hide keyboard when first visible item is bigger then 0`(firstVisibleItem: Int) {
        presenter.onCurrenciesScrolled(firstVisibleItem, view)

        verify(view).hideKeyboard()
    }

    @Test
    fun `do nothing when first visible item is 0`() {
        presenter.onCurrenciesScrolled(0, view)

        verifyZeroInteractions(view)
    }

    @Test
    fun `do nothing after view detached`() {
        presenter.attachView(view)

        presenter.detachView()

        rates.onNext(ratesResult)
        rates.onNext(RatesResult.Error)
        rates.onNext(ratesResult)

        verify(view).setProgress(true)
        verifyNoMoreInteractions(view)
    }

    companion object {

        val baseCurrency = Currency(CurrencySymbol.EUR, BigDecimal.ONE)
        val plnCurrencyUpdate = Currency(symbol = CurrencySymbol.PLN, value = 2.99.toBigDecimal())
        val usdCurrencyUpdate = Currency(symbol = CurrencySymbol.USD, value = 1.21.toBigDecimal())
        val cnyCurrencyUpdate = Currency(symbol = CurrencySymbol.CNY, value = 2.74.toBigDecimal())


        val ratesResult = RatesResult.Ok(
            Rates(
                base = baseCurrency,
                currencies = listOf(
                    plnCurrencyUpdate,
                    usdCurrencyUpdate,
                    cnyCurrencyUpdate
                )
            )
        )
    }
}