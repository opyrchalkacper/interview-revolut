package com.opyrchalkacper.interview.revolut

import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.junit.jupiter.api.extension.AfterEachCallback
import org.junit.jupiter.api.extension.BeforeEachCallback
import org.junit.jupiter.api.extension.ExtensionContext

open class RxRule @JvmOverloads constructor(
    private val testScheduler: Scheduler = instantScheduler()
) : BeforeEachCallback, AfterEachCallback {

    override fun beforeEach(context: ExtensionContext?) {
        resetSchedulers()
        overrideSchedulers(testScheduler)
    }

    override fun afterEach(context: ExtensionContext?) {
        resetSchedulers()
    }

    private fun overrideSchedulers(scheduler: Scheduler) {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler }
        RxAndroidPlugins.setMainThreadSchedulerHandler { scheduler }
        RxJavaPlugins.setComputationSchedulerHandler { scheduler }
        RxJavaPlugins.setIoSchedulerHandler { scheduler }
        RxJavaPlugins.setNewThreadSchedulerHandler { scheduler }
        RxJavaPlugins.setSingleSchedulerHandler { scheduler }

        RxJavaPlugins.setErrorHandler {}
    }

    private fun resetSchedulers() {
        RxAndroidPlugins.reset()
        RxJavaPlugins.reset()
    }

}

private fun instantScheduler() =
    object : Scheduler() {
        override fun createWorker() = ExecutorScheduler.ExecutorWorker(Runnable::run, false)
    }